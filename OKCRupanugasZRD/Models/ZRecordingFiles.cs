﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OKCRupanugasZRD.Models
{
    public class ZRecordingFiles
    {
		public string Id { get; set; }

		public string Meeting_Id { get; set; }

		public DateTime Recording_Start { get; set; }

		public DateTime Recording_End { get; set; }

		public string File_Type { get; set; }

		public string File_Extension { get; set; }

		public int File_Size { get; set; }

		public string Download_Url { get; set; }

		public string Status { get; set; }

		public string Recording_Type { get; set; }
	}
}
