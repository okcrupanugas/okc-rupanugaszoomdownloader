﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog;
using NLog.Extensions.Logging;
using OKCRupanugasZRD.Models;

namespace OKCRupanugasZRD
{
    internal class Program
    {
		private static IConfigurationRoot config;

		private static async Task Main(string[] args)
		{
			try
			{
				config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
				BuildDi(config);
				IConfigurationSection zoomApiConfigSection = config.GetSection("zoomApi");
				Console.WriteLine("Sending Request -----------------------------------------");
				string ResponseString = string.Empty;
				string baseUrl = zoomApiConfigSection.GetValue<string>("apiUrl");
				int limtSize = zoomApiConfigSection.GetValue<int>("fileLimtSize");
				string audioFormat = zoomApiConfigSection.GetValue<string>("audioFormat");
				string outputFolder = zoomApiConfigSection.GetValue<string>("downloadFolderPath");
				string meetingsUrl = zoomApiConfigSection.GetValue<string>("meetingsUrl");
				bool delete = zoomApiConfigSection.GetValue<bool>("deleteRecordingAfterDownload");
				Logger log = LogManager.GetCurrentClassLogger();
				using (new HttpClient())
				{
					string token = GetToken();
					string deleteUrl = "meetings/{0}/recordings/{1}";
					HttpWebRequest obj = (HttpWebRequest)WebRequest.Create(baseUrl + meetingsUrl + "&to=" + DateTime.Now.Date.ToString("yyyy-MM-dd"));
					obj.Accept = "application/json";
					obj.Method = "GET";
					log.Info("Start Fetching Recordings");
					obj.Headers.Set("Authorization", "Bearer " + token);
					using (HttpWebResponse response3 = (HttpWebResponse)obj.GetResponse())
					{
						ResponseString = new StreamReader(response3.GetResponseStream()).ReadToEnd();
					}
					log.Debug("Data: " + ResponseString);
					log.Info("End Fetching Recordings");
					IEnumerable<ZRecordingFiles> enumerable = JsonConvert.DeserializeObject<RootRecordings>(ResponseString).Meetings.SelectMany((Meetings x) => x.Recording_Files);
					enumerable.Where((ZRecordingFiles x) => x.Recording_Type != audioFormat);
					foreach (ZRecordingFiles rec in enumerable)
					{
						if (rec.Recording_Type != audioFormat && rec.File_Size >= limtSize)
						{
							log.Info("Start - Downloading recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
							WebClient webClient = new WebClient();
							try
							{
								string outputpath = Path.Combine(outputFolder, rec.Id + "." + rec.File_Extension);
								webClient.DownloadFile(rec.Download_Url + "?access_token=" + token, outputpath);
							}
							catch (Exception ex3)
							{
								log.Error("Downloading recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
								log.Error(ex3);
							}
							finally
							{
								((IDisposable)webClient)?.Dispose();
							}
							log.Info("End - Downloading recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
						}
						if (!delete)
						{
							continue;
						}
						log.Info("Deleting - Recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
						HttpWebRequest deleteRecordingRequest = (HttpWebRequest)WebRequest.Create(string.Format(baseUrl + deleteUrl, rec.Meeting_Id, rec.Id));
						deleteRecordingRequest.Accept = "application/json";
						deleteRecordingRequest.Method = "DELETE";
						deleteRecordingRequest.Headers.Set("Authorization", "Bearer " + token);
						try
						{
							using HttpWebResponse response2 = (HttpWebResponse)deleteRecordingRequest.GetResponse();
							new StreamReader(response2.GetResponseStream()).ReadToEnd();
						}
						catch (Exception ex2)
						{
							log.Error("Deleting -Recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
							log.Error(ex2);
						}
						log.Info("Deleted -Recording for meeting Id- {0} with Recording Id- {1}", rec.Meeting_Id, rec.Id);
					}
					log.Info("Finished Downloading available recordings");
					Environment.Exit(0);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR-" + ex.ToString());
			}
		}

		private static string GetToken()
		{
			IConfigurationSection section = config.GetSection("zoomApi");
			string accessToken = string.Empty;

            var client = new HttpClient();
			client.BaseAddress = new Uri("https://zoom.us/oauth/token");
			var request = new HttpRequestMessage(HttpMethod.Post, string.Empty);
			request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
				Encoding.ASCII.GetBytes($"{section.GetValue<string>("clientId")}:{section.GetValue<string>("clientSecret")}")
				));
			request.Content = new FormUrlEncodedContent(new Dictionary<string, string>
																{
																	{ "grant_type", "account_credentials" },
																	{ "account_id", section.GetValue<string>("accountId") }

																});
			request.Headers.Host = "zoom.us";

			var response = client.SendAsync(request).Result;

			if (response.IsSuccessStatusCode)
			{
				var content = response.Content.ReadAsStringAsync().Result;
				var token = JsonConvert.DeserializeObject<ZoomAccessToken>(content);
				accessToken = token.access_token;

			}
			else
			{
				// Handle the error
				Console.WriteLine($"Failed to retrieve access token: {response.ReasonPhrase}");

			}

			return accessToken;
		}
        
        private static IServiceProvider BuildDi(IConfiguration config)
		{
			return new ServiceCollection().AddLogging(delegate (ILoggingBuilder loggingBuilder)
			{
				loggingBuilder.ClearProviders();
				loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
				loggingBuilder.AddNLog("nlog.config");
			}).BuildServiceProvider();
		}
	}
}
