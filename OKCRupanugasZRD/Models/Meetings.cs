﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OKCRupanugasZRD.Models
{
    public class Meetings
    {
		public string Uuid { get; set; }

		public long Id { get; set; }

		public string Account_Id { get; set; }

		public string Host_Id { get; set; }

		public string Topic { get; set; }

		public List<ZRecordingFiles> Recording_Files { get; set; }
	}
}
